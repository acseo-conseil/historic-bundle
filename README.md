# ACSEO Historic Bundle (Symfony bundle)

This bundle is used to historize user actions on objects. It allows you to choose a log label depending on many contexts ! You can also attach historic objects to a particular entity, and get entries from it.

This bundle is a different way to analyse doctrine event like Gedmo does.

It’s realy easy to learn it, and you can specify many behaviors, like define some request contexts to write particular log, or choose another target to receive the entry...  

Exemple:

You have an application which has a user change password form. You want to log this changement with a particular historic label. In your mind, you maybe think that you can historize the change of email also in other part of your website, perhaps when user registers an address or creates a comment on a topic. With Acseo Historic, you can do something like this with a simple yaml configuration file:

``` yml
    AppBundle\Entity\User:
        edit:
            listen_properties:
                - property: password
                  label: change password
```

A new entry will be created with label « change password » on edit user context. You can attach this log to the entity concerned by an historic object relation, customize the label at your convinience, it's all up to you.

## Installation

First, add the bundle to your dependencies in your composer.json :
``` json
{
    "require": {
        // Your config +
        "acseo/historic-bundle": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.acseo.co/acseoteam/historic-bundle.git"
        }
    ],
}
```

Install it with Composer in the terminal :
``` shell
    $ composer update
```

Then, enable the bundle in you AppKernel.php :
``` php
    class AppKernel extends Kernel
    {
        public function registerBundles()
        {
            $bundles = [
                // Your config
                new Acseo\HistoricBundle\AcseoHistoricBundle(),
            ];

            ...
```

Put your bundle config in app/config.yml :
``` yml
    acseo_historic:
        config_path: ../src/AppBundle/Resources/config/historic.yaml  # This is an exemple where you can put your historic config file
        user_class: AppBundle\Entity\User                             # The user class used in your application
```

Finally, update your database schema:
```shell
$ bin/console doctrine:schema:update
```

## Basic Usage

The bundle is now ready to go. Go in your configuration file (historic.yaml) and write:

``` yml
    historize:
        Full\Namespace\Entity: ~
```

Now, manipulate your entities with this class and look at the acseo_historic table changes,
It will log every action on this object.

__________________________________________________________________________________________________________

## Basic Options:


#### Label : string

The label of log


#### stop_propagation: boolean

Stop process and don’t write log.


#### create: array
#### edit:     array
#### delete: array

Define special option to use on particular of entity life context


#### request_contexts: array

You can let you redefine all option on context request. It depends of a request attribute like route name.


## Begin by exemples

``` yml
    historize:
        AppBundle\Entity\Comments:
            label: this object has been edited
            create:
                label: object created
```

In this exemple, we define in all life object context the same label (« this object has been edited ») but we defined that in create context, we save a log with « object created ».

``` yml
    historize:
        AppBundle\Entity\Comments:
            label: this object has been edited
            create:
                label: object created
            delete:
                stop_propagation: true
```

Here the same exemple but we don’t want to historize the delete action on this object.

Ok, now we know that comment entity can be create from two part of our website, from the route app_info_comments, and app_topic_comments. But you want to historize only one of them. So:

``` yml
    historize:
        AppBundle\Entity\Comments:
            label: this object has been edited
            create:
                label: object created
            delete:
                stop_propagation: true
            request_contexts:
                _route:
                    value: app_topic_comments
                    create:
                        label: comment created on topic
                _route:
                    value: app_info_comments
                    stop_propagation: true
```

Here, we defined a default behavior,  but if object change from app_info_comments route, no entry will be created, but created from app_topic_comments, we save a log with « comment created on topic ».
And, if object is edited for other part of your app, we save a log with label « object created ».

In reality, the system analyse the request attributes. So, if you have a particuliar route you want to listen for special entities, you can test the route context. This is juste because the code do that :

```php
If ($request->get($yourParam) == $yourValue){
    // redefine options
}
```

$yourParam and $yourValue is what you define in the yaml file config:

``` yml
    request_contexts:
        _route:                                    # this mean if($request->get(_route) == 'app_topic_comments'){        
            value: app_topic_comments              #                   // on create do this etc..    
            create:                                #           }
                label: comment created
```

## Attach log to an entity:

To create a relation beetween historic object and the entity listen, just write it in the entity concerned :

``` php
    class Entity
    {
        use \Acseo\HistoricBundle\Traits\HistoricTrait;
    }
```

And update you database schema:

``` shell
    $ php bin/console doctrine:schema:update -f
```

Thats all. Now you can work historic collection from your object.

The bundle give you a base template to show historic collection of an object, in a table adapt for bootstrap, in the template AcseoHistoricBundle:Base:base_bootstrap_table.html.twig

Options can be set when you render this template:

#### classTh : class for "< th >"
#### classTd : class for "< td >"
#### object : object concerned which use HistoricTrait

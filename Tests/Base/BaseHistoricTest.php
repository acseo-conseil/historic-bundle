<?php

namespace Acseo\HistoricBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Acseo\HistoricBundle\Base\BaseHistoric;
use Acseo\HistoricBundle\Entity\Historic;
use Acseo\HistoricBundle\Interfaces\HistoricInterface;

/**
 * BaseHistoricTest
 */
class BaseHistoricTest extends WebTestCase
{
    /**
     * test buildHistoric
     *
     * This test save in your acseo_historic table 4 logs for entity test changements :
     * "Create Object", "Edit Object", "Edit action of object", "Delete object"
     */
    public function testHistoric()
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $em = $container->get('doctrine')->getManager();
        $translator = $container->get('translator.default');

        $historizer = new BaseHistoric($em, null, $translator, null, []);

        // create Entity test
        $historic = new Historic('ENTITY TEST');
        $em->persist($historic);
        $em->flush();
        // set mode create
        $historizer->action = HistoricInterface::ACTION_CREATE;
        // try to save the create log of this Historic entity
        $this->assertTrue($historizer->onCreate($historic, null));
        // set mode edit
        $historizer->action = HistoricInterface::ACTION_EDIT;
        // historic is save, try to edit it and listen property action to get 2 logs on edit
        $historizer->setOption('listen_properties', [['property' => 'action']]);

        $historic->setAction('ENTITY TEST EDIT');
        // construct uow changements like doctrine does
        $doctrineUOWChangements = [
            'action' => [0 => 'ENTITY TEST', 1 => 'ENTITY TEST EDIT'],
        ];

        $this->assertTrue($historizer->onEdit($historic, [], $doctrineUOWChangements, null));
        // set mode delete
        $historizer->action = HistoricInterface::ACTION_DELETE;
        // ACTION DELETE IS ON PREREMOVE EVENT SO WE CANT TEST IN REAL SITUATION BUT TEST LIKE THIS
        $this->assertTrue($historizer->onDelete($historic, null));
        // Remove object
        $em->remove($historic);
        $em->flush();
    }
}

<?php

namespace Acseo\HistoricBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Acseo\HistoricBundle\Analyzer\HistoricAnalyzer;
use Acseo\HistoricBundle\Entity\Historic;
use Acseo\HistoricBundle\Interfaces\HistoricInterface;

/**
 * HistoricAnalyzerTest
 */
class HistoricAnalyzerTest extends WebTestCase
{
    /**
     * test Analyze of historic conf declaration
     *
     * This test save in your acseo_historic table 3 logs for entity test changements
     * "Create Object", "Edit Object", "Edit action of object"
     */
    public function testAnalyze()
    {
        $client = static::createClient();
        $client->request('GET', '/');
        $container = $client->getContainer();

        $em = $container->get('doctrine')->getManager();
        $security = $container->get('security.token_storage');
        $request = $container->get('request_stack');
        // push request into request_stack container
        $request->push($client->getRequest());
        $translator = $container->get('translator.default');

        $analyzer = new HistoricAnalyzer($em, $security, $request, $translator);
        $analyzer->setParameters(['Acseo\\HistoricBundle\\Entity\\Historic' => null]);

        // Check if analyzer catch this not declared class
        $this->assertFalse($analyzer->analyze($analyzer, HistoricInterface::ACTION_CREATE));
        $this->assertFalse($analyzer->analyze($analyzer, HistoricInterface::ACTION_EDIT));
        $this->assertFalse($analyzer->analyze($analyzer, HistoricInterface::ACTION_DELETE));

        $historic = new Historic('test');
        $historic->setClass('phpunit create TEST');

        $em->persist($historic);
        $em->flush();
        // We want to listen an entity, we choose acseo historic entity with BaseHistoric service
        $this->assertTrue($analyzer->analyze($historic, HistoricInterface::ACTION_CREATE));
        // change class attribute
        $historic->setClass('phpunit edit TEST');
        $this->assertTrue($analyzer->analyze($historic, HistoricInterface::ACTION_EDIT));
        // ACTION DELETE IS ON PREREMOVE EVENT SO WE CANT TEST IN REAL SITUATION BUT TEST LIKE THIS
        $this->assertTrue($analyzer->analyze($historic, HistoricInterface::ACTION_DELETE));
        // Remove object
        $em->remove($historic);
        $em->flush();
    }
}

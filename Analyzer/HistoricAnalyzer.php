<?php

namespace Acseo\HistoricBundle\Analyzer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Yaml\Yaml;
use Acseo\HistoricBundle\Interfaces\HistoricAnalyzerInterface;
use Symfony\Component\Translation\Translator;

/**
 * HistoricAnalyzer.
 *
 * This class analyze if object is defined in historic.yaml
 * Then she instanciates the service choosen and lunch the method buildHistoric()
 * defined into CoreBundle\Service\Historic\Base\HistoricInterface
 * which run function onCreate() onEdit() onDelete() depends of action
 */
class HistoricAnalyzer implements HistoricAnalyzerInterface
{
    const BASE_HISTORIC = 'Acseo\HistoricBundle\Base\BaseHistoric';
    const HISTORIC_INTERFACE = 'Acseo\HistoricBundle\Interfaces\HistoricInterface';

    // UTILITIES
    private $em;
    private $security;
    private $request;
    private $translator;

    // array of classes the analyzer have to listen
    private $parameters;
    // YAML file path parsed and inject into $this->parameters
    private $configPath;
    // for logs all entities (not used yet)
    private $listenAll;

    /**
     * Constuctor.
     */
    public function __construct(EntityManagerInterface $em, TokenStorage $security, RequestStack $request, Translator $translator, $configPath = null, $listenAll = null)
    {
        $this->em = $em;
        $this->security = $security;
        $this->request = $request->getMasterRequest();
        $this->translator = $translator;
        $this->configPath = $configPath;
        $this->listenAll = $listenAll;
    }

    /**
     * {@inheritdoc}
     */
    public function analyze($object, $action)
    {
        $this->hydrateParameters();
        // you can start your Yaml file by historize: or just declare definition
        $params = is_array($this->parameters) && array_key_exists('historize', $this->parameters)
            ? $this->parameters['historize']
            : $this->parameters
        ;

        if ($params && is_array($params) && count($params) > 0) {
            foreach ($params as $class => $data) {
                if ($object instanceof $class) {
                    $class = $this->getServiceClass($data);
                    // Historic instance
                    $historicService = new $class($this->em, $this->request, $this->translator, $action, $data);
                    // check if service implements HistoricInterface
                    $this->checkService($historicService);
                    // start process
                    $historicService->buildHistoric(
                        $object,
                        $this->em->getUnitOfWork()->getOriginalEntityData($object),
                        $this->em->getUnitOfWork()->getEntityChangeSet($object),
                        is_object($this->security->getToken()->getUser()) ? $this->security->getToken()->getUser() : null,
                        $action
                    );

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * hydrateParameters.
     */
    private function hydrateParameters()
    {
        if ($this->configPath && file_exists($this->configPath)) {
            $this->parameters = Yaml::parse(file_get_contents($this->configPath));
        }
    }

    /**
     * getServiceClass.
     *
     * @param mixed $string
     *
     * @return string|null
     */
    private function getServiceClass($data)
    {
        return !is_array($data) || !array_key_exists('service', $data) || !$data['service'] || '' == $data['service'] ? static::BASE_HISTORIC : $data['service'];
    }

    /**
     * checkService - check if service implements right interface.
     *
     * @param mixed $service
     *
     * @return null|throw \Exception
     */
    private function checkService($service, $historicInterface = self::HISTORIC_INTERFACE)
    {
        if (!$service instanceof $historicInterface) {
            throw new \Exception('Your historic service must implements '.$historicInterface);
        }

        return true;
    }

    /**
     * setParameters
     *
     * @param array|null $parameters
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * getParameters
     *
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }
}

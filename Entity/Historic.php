<?php

namespace Acseo\HistoricBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historic.
 *
 * @ORM\Table(name="acseo_historic")
 * @ORM\Entity(repositoryClass="Acseo\HistoricBundle\Repository\HistoricRepository")
 */
class Historic
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=100, nullable=true)
     */
    private $action;

    /**
     * @var int
     *
     * @ORM\Column(name="objectId", type="integer", nullable=true)
     */
    private $objectId;

    /**
     * @var string
     *
     * @ORM\Column(name="class", type="string", length=80, nullable=true)
     */
    private $class;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    // Relation is load dynamically
    private $user;

    /**
     * Constructor.
     */
    public function __construct($action = null, $user = null, $objectId = null, $class = null)
    {
        $this->action = $action;
        $this->user = $user;
        $this->objectId = $objectId;
        $this->class = $class;
        $this->date = new \Datetime('now');
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set action.
     *
     * @param string $action
     *
     * @return Historic
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action.
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Historic
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user.
     *
     * @param \UserInterface $user
     *
     * @return Historic
     */
    public function setUser($user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set objectId.
     *
     * @param int $objectId
     *
     * @return Historic
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;

        return $this;
    }

    /**
     * Get objectId.
     *
     * @return int
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * Set class.
     *
     * @param string $class
     *
     * @return Historic
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class.
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }
}

<?php

namespace Acseo\HistoricBundle\Base;

/**
 * Historic.
 *
 * This class is a base historic service class
 * used to build historic of entities
 */
class BaseHistoric extends AbstractHistoric
{
}

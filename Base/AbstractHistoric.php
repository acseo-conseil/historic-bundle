<?php

namespace Acseo\HistoricBundle\Base;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Translator;
use Acseo\HistoricBundle\Entity\Historic;
use Acseo\HistoricBundle\Interfaces\HistoricInterface;
use Acseo\HistoricBundle\Traits\HistoricOptionsTrait;

/**
 * AbstractHistoric.
 *
 * This class help to historize the object
 *
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
abstract class AbstractHistoric implements HistoricInterface
{
    // UTILS
    protected $em;
    protected $request;
    protected $translator;
    // Action context
    public $action;
    // options
    use HistoricOptionsTrait;

    /**
     * Constuctor.
     */
    public function __construct(EntityManagerInterface $em, Request $request = null, Translator $translator, $action, $options)
    {
        $this->em = $em;
        $this->request = $request;
        $this->translator = $translator;
        $this->action = $action;
        $this->hydrateOptions($options, $action);
    }

    /**
     * {@inheritdoc}
     */
    public function onCreate($object, $user)
    {
        return $this->baseProcess($object, $user, static::CREATE_LABEL);
    }

    /**
     * {@inheritdoc}
     */
    public function onEdit($object, $originalData, $modifs, $user)
    {
        $collectionUpdates = $this->em->getUnitOfWork()->getScheduledCollectionUpdates();
        // if a child entity fire prePersist event
        // because add a new historic entry in his parent
        foreach ($collectionUpdates as $peristentCollection) {
            foreach ($peristentCollection as $fields) {
                if ($fields instanceof Historic) {
                    return false;
                }
            }
        }

        return $this->baseProcess($object, $user, static::EDIT_LABEL, $modifs);
    }

    /**
     * {@inheritdoc}
     */
    public function onDelete($object, $user)
    {
        return $this->baseProcess($object, $user, static::DELETE_LABEL);
    }

    /**
     * baseProcess.
     *
     * @param mixed        $object
     * @param UserInteface $user
     * @param string       $label
     */
    private function baseProcess($object, $user, $label = null, $modifs = [])
    {
        $this->checkRequestContext($this->action);

        if ($this->getOption('stop_propagation')) {
            return false;
        }

        $this->setOption('label', (null == $this->getOption('label') ? $label : $this->getOption('label')));

        $target = $this->findTarget($object);
        !$this->getOption('listen_properties') ?: $this->listenProperties($modifs, ($target ? $target : $object), $user, $object->getId());

        if ($this->getOption('stop_propagation')) {
            return false;
        }

        $historic = $target
            ? $this->createHistoricEntity($target, $user, $this->getOption('label'), $target->getId(), $this->getClass($target))
            : $this->createHistoricEntity($object, $user, $this->getOption('label'))
        ;

        $this->setOption('label', null);
        $this->save($historic);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function buildHistoric($object, $originalData, $modifs, $user, $action)
    {
        if (!$object) {
            return false;
        }

        switch ($action) {
            case static::ACTION_CREATE:
                return $this->onCreate($object, $user);
            break;
            case static::ACTION_EDIT:
                 return $this->onEdit($object, $originalData, $modifs, $user);
            break;
            case static::ACTION_DELETE:
                return $this->onDelete($object, $user);
            break;
        }

        return false;
    }

    /**
     * checkContext.
     *
     * @return bool
     */
    private function checkRequestContext($action)
    {
        foreach ($this->getOption('request_contexts') as $requestKey => $options) {
            if ($this->request && $this->request->get($requestKey) == $options['value']) {
                $this->hydrateOptions($options, $action);
                break;
            }
        }
    }

    /**
     * listenProperties.
     *
     * @param array        $modifs
     * @param mixed        $object
     * @param UserInteface $user
     * @param int          $id
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    private function listenProperties($modifs, $object, $user, $id)
    {
        if (!$this->getOption('listen_properties') || 0 == count($this->getOption('listen_properties'))) {
            return;
        }

        if ($this->getOption('listen_properties') && is_array($this->getOption('listen_properties'))) {
            foreach ($this->getOption('listen_properties') as $data) {
                // listen collection
                if (!is_array($data)) {
                    throw new \Exception('listen_propeties option expects array arguments');
                }
                if (array_key_exists('is_collection', $data) && true == $data['is_collection']) {
                    $collectionUpdates = $this->em->getUnitOfWork()->getScheduledCollectionUpdates();
                    foreach ($collectionUpdates as $collection) {
                        $parent = $collection->getOwner();
                        if ($parent && array_key_exists('property', $data) && null !== $data['property']) {
                            $method = $this->constructGetterName($data['property']);
                            $dataPropety = method_exists($object, $method) ? $object->$method() : null;
                            if ($dataPropety) {
                                if (count($dataPropety->getSnapShot()) !== count($dataPropety)) {
                                    $this->constructSubHistoric($data, $object, $field, $user, $id);
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    // listen field
                    foreach ($modifs as $field => $changes) {
                        if (array_key_exists('property', $data) && $data['property'] == $field) {
                            $this->constructSubHistoric($data, $object, $field, $user, $id);
                            break;
                        }
                    }
                }
            }
        }
    }

    /**
     * constructSubHistoric for listen property option.
     *
     * @param array         $data
     * @param mixed         $object
     * @param string        $field
     * @param UserInterface $user
     * @param integer       $id
     */
    private function constructSubHistoric($data, $object, $field, $user, $id)
    {
        $label = array_key_exists('label', $data) ? $data['label'] : ucfirst($this->action).' "'.$field.'" of object';

        !array_key_exists('translate', $data) || true !== $data['translate'] ?: $label = $this->translator->trans($label, [], 'acseo_historic');

        $subHistoric = $this->createHistoricEntity($object, $user, $label, $id, $this->getClass($object));

        $this->em->persist($subHistoric);

        if (array_key_exists('stop_propagation', $data) && true == $data['stop_propagation']) {
            $this->em->flush();
            $this->setOption('stop_propagation', true);
        }
    }

    /**
     * constructGetterName method.
     *
     * @param string $method
     *
     * @return string
     */
    private function constructGetterName($method)
    {
        return 'get'.ucfirst($method);
    }

    /**
     * createHistoricEntity.
     *
     * Call addHistoricToObject() when attach $historic and $object
     *
     * @param mixed        $object
     * @param UserInteface $user
     * @param string       $label
     * @param int          $id     (object which change)
     *
     * @return Historic
     */
    protected function createHistoricEntity($object = null, $user, string $label, $id = null, $objectClass = null)
    {
        $objectClass = $objectClass ? $objectClass : $this->getClass($object);
        $id = $id ? $id : $object->getId();

        $historic = new Historic($this->constructActionLabel($label), $user, $id, $objectClass);

        $this->addHistoricToObject($object, $historic);

        return $historic;
    }

    /**
     * constructActionLabel.
     *
     * @param string $label
     *
     * @return string
     */
    protected function constructActionLabel($label)
    {
        return true == $this->getOption('translate') ? $this->translator->trans($label, [], 'acseo_historic') : $label;
    }

    /**
     * addHistoricToObject.
     *
     * Can be override to chose another
     * $object target to add historic entry
     *
     * @param mixed         $object
     * @param Historic|null $historic
     */
    protected function addHistoricToObject($object, $historic)
    {
        if ($object && $this->getOption('subEntityField')) {
            $this->getSubEntity($object)->addHistoric($historic);

            return;
        }

        !method_exists($object, 'addHistoric') ?: $object->addHistoric($historic);
    }

    /**
     * getSubEntity - Called for subEntityField.
     *
     * @param mixed $object
     *
     * @return object
     */
    protected function getSubEntity($object)
    {
        if (!$this->getOption('subEntityField')) {
            throw new \Exception('You need to choose a sub entity field');
        }

        $method = $this->constructGetterName($this->getOption('subEntityField'));

        if (!method_exists($object, $method)) {
            throw new \Exception('Entity '.get_class($object).' doesn\'t have method name '.$method);
        }

        $subEntity = $object->$method();

        if ($subEntity && !method_exists($subEntity, 'addHistoric')) {
            throw new \Exception('Entity '.get_class($object).' has to implements Acseo\HistoricBundle\Traits\HistoricTrait.');
        }

        return $subEntity;
    }

    /**
     * save historic.
     *
     * No flush on delete because event is preRemove
     *
     * @param Historic $historic
     */
    protected function save(Historic $historic)
    {
        $this->em->persist($historic);
        $this->action == static::ACTION_DELETE ?: $this->em->flush();
    }

    /**
     * getClass.
     *
     * @param mixed $object
     *
     * @return string
     */
    private function getClass($object)
    {
        return $this->em->getClassMetadata(get_class($object))->getName();
    }

    /**
     * findTarget.
     *
     * @return null|object|\Exception
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    private function findTarget($object)
    {
        $target = null;

        if ($this->getOption('find_target_by') && is_array($this->getOption('find_target_by')) && count($this->getOption('find_target_by')) > 0) {
            foreach ($this->getOption('find_target_by') as $param) {
                if (!is_array($param)) {
                    throw new \Exception('find_target_by option expects array arguments', 1);
                }
                if (array_key_exists('stop_propagation', $param) && true == $param['stop_propagation']) {
                    $this->setOption('stop_propagation', true);

                    return $target;
                }
                if (array_key_exists('repository', $param) && array_key_exists('field', $param)) {
                    $hasContext = array_key_exists('search_by_getter', $param);
                    $function = !$hasContext ?: $this->constructGetterName($param['search_by_getter']);

                    $field = $hasContext && method_exists($object, $function) ? $object->$function() : $param['field'];

                    $qb = $this->em->getRepository($param['repository'])->createQueryBuilder('a');
                    $entity = $qb
                        ->leftJoin('a.'.$field, $field)
                        ->where($qb->expr()->in($field.'.id', [ $object->getId() ]))
                        ->getQuery()
                        ->getResult()
                    ;

                    $target = count($entity) ? $entity[0] : $target;

                    if ($target) {
                        if (array_key_exists('label', $param)) {
                            $this->label = $param['label'];
                        }
                        break;
                    }
                } else {
                    throw new \Exception('You must choose a repository AND field expected for find_target_by option !');
                }
            }
        }

        return $target;
    }
}

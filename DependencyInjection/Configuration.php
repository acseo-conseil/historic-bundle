<?php

namespace Acseo\HistoricBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Configuration of DependencyInjection.
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('acseo_historic');

        $rootNode
            ->children()
                ->scalarNode('config_path')
                    ->info('This value is the path of historic configuration file (Yaml)')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('user_class')
                    ->info('The user class used to create relation')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->booleanNode('listen_all')
                    ->defaultFalse()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}

<?php

namespace Acseo\HistoricBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;

/**
 * HistoricExtension.
 */
class HistoricExtension extends Extension
{
    /**
     * load.
     *
     * @param array            $configs
     * @param ContainerBuilder $container
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );

        $loader->load('services.yml');

        $container->setParameter('acseo_historic.config_path', $config['config_path']);
        $container->setParameter('acseo_historic.user_class', $config['user_class']);
        $container->setParameter('acseo_historic.listen_all', $config['listen_all']);

        // add to cache this classes which used many times
        $this->addAnnotatedClassesToCompile(array(
            'Acseo\\HistoricBundle\\Analyzer\\HistoricAnalyzer',
            'Acseo\\HistoricBundle\\Base\\AbstractHistoric',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespace()
    {
        return 'http://acseo.fr';
    }

    /**
     * {@inheritdoc}
     */
    public function getXsdValidationBasePath()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getAlias()
    {
        return 'acseo_historic';
    }
}

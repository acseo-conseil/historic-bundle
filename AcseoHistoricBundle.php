<?php

namespace Acseo\HistoricBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Acseo\HistoricBundle\DependencyInjection\HistoricExtension;

/**
 * HistoricBundle.
 */
class AcseoHistoricBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new HistoricExtension();
        }

        return $this->extension;
    }
}

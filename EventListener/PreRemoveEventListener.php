<?php

namespace Acseo\HistoricBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Acseo\HistoricBundle\Analyzer\HistoricAnalyzer;
use Acseo\HistoricBundle\Interfaces\HistoricInterface;

/**
 * PreRemoveEventListener.
 */
class PreRemoveEventListener
{
    // historic analyser
    private $historicAnalyzer;

    /**
     * Constructor.
     */
    public function __construct(HistoricAnalyzer $historicAnalyzer)
    {
        $this->historicAnalyzer = $historicAnalyzer;
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        $data = $args->getObject();

        // Analyse object historic
        $this->historicAnalyzer->analyze($data, HistoricInterface::ACTION_DELETE);
    }
}

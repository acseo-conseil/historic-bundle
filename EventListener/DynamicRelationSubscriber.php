<?php

namespace Acseo\HistoricBundle\EventListener;

use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;

/**
 * DynamicRelationSubscriber.
 */
class DynamicRelationSubscriber implements EventSubscriber
{
    private $userClass;

    /**
     * Constructor.
     */
    public function __construct($userClass)
    {
        $this->userClass = $userClass;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return array(
            Events::loadClassMetadata,
        );
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        // the $metadata is the whole mapping info for this class
        $metadata = $eventArgs->getClassMetadata();

        if ('Acseo\HistoricBundle\Entity\Historic' != $metadata->getName()) {
            return;
        }

        if ($this->userClass) {
            $metadata->mapManyToOne(array(
                'targetEntity' => $this->userClass,
                'fieldName' => 'user',
            ));
        }
    }
}

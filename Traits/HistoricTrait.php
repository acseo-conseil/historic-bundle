<?php

namespace Acseo\HistoricBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * HistoricTrait.
 *
 * Use to create relation with historic table into target entity
 */
trait HistoricTrait
{
    /**
     * @ORM\ManyToMany(targetEntity="\Acseo\HistoricBundle\Entity\Historic", cascade={"persist"})
     */
    private $historics;

    /**
     * Constructor.
     */
    public function __construct()
    {
        !method_exists(parent::class, '__construct') ?: parent::__construct();

        $this->historics = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add historic.
     *
     * @param \Acseo\HistoricBundle\Entity\Historic $historic
     *
     * @return Client
     */
    public function addHistoric(\Acseo\HistoricBundle\Entity\Historic $historic)
    {
        $this->historics[] = $historic;

        return $this;
    }

    /**
     * Remove historic.
     *
     * @param \Acseo\HistoricBundle\Entity\Historic $historic
     */
    public function removeHistoric(\Acseo\HistoricBundle\Entity\Historic $historic)
    {
        $this->historics->removeElement($historic);
    }

    /**
     * Get historics.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistorics()
    {
        return $this->historics;
    }
}

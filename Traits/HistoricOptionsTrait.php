<?php

namespace Acseo\HistoricBundle\Traits;

/**
 * HistoricOptionsTrait.
 *
 * Manage options of BaseHistoric
 */
trait HistoricOptionsTrait
{
    public $options = [];

    /**
     * setOption.
     *
     * @param string $key
     * @param mixed  $value
     */
    public function setOption($key, $value)
    {
        $this->options[$key] = $value;
    }

    /**
     * resetOptions.
     *
     * @param null|array  $value
     */
    public function resetOptions()
    {
        $this->options = [];
    }

    /**
     * getOption.
     *
     * @param mixed $key
     *
     * @return mixed
     */
    public function getOption($key)
    {
        return array_key_exists($key, $this->options) ? $this->options[$key] : null;
    }

    /**
     * getOptions.
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * hydrateOptions.
     *
     * @param array  $options
     * @param string $action
     */
    private function hydrateOptions($options, $action)
    {
        $this->setOption('label', $this->extractOption($options, 'label', $action));
        $this->setOption('translate', $this->extractOption($options, 'translate', $action));
        $this->setOption('subEntityField', $this->extractOption($options, 'subEntityField', $action));
        $this->setOption('find_target_by', $this->extractOption($options, 'find_target_by', $action));
        $this->setOption('stop_propagation', $this->extractOption($options, 'stop_propagation', $action));
        $this->setOption('listen_properties', $this->extractOption($options, 'listen_properties', $action));
        // REQUEST CONTEXT OPTION
        $requestContexts = $this->extractOption($options, 'request_contexts', $action);
        $this->setOption('request_contexts', $requestContexts ? $requestContexts : []);
    }

    /**
     * extractOption.
     *
     * @param array  $options
     * @param string $key
     * @param string $action
     *
     * @return mixed
     */
    private function extractOption($options, $key, $action)
    {
        $option = null;

        is_array($options) && array_key_exists($action, $options) && is_array($options[$action]) && array_key_exists($key, $options[$action])
            ? $option = $options[$action][$key]
            : $option = is_array($options) && array_key_exists($key, $options) && null !== $options[$key] ? $options[$key] : null
        ;

        return $option;
    }
}

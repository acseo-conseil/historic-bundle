<?php

namespace Acseo\HistoricBundle\Interfaces;

/**
 * HistoricInterface.
 *
 * This interface is used to defined function used
 * to create historic entry through the doctrine event cycle
 */
interface HistoricInterface
{
    // CRUD PREFIX
    const CREATE_LABEL = 'Create object';
    const EDIT_LABEL = 'Edit object';
    const DELETE_LABEL = 'Delete object';

    // CRUD CONTEXT
    const ACTION_CREATE = 'create';
    const ACTION_EDIT = 'edit';
    const ACTION_DELETE = 'delete';

    /**
     * onCreate $object.
     *
     * This function defined historic to save on create doctrine event
     *
     * @param mixed         $object (has to use HistoricTrait)
     * @param UserInterface $user
     *
     * @return boolean      success or failed
     */
    public function onCreate($object, $user);

    /**
     * onEdit $object.
     *
     * This function defined historic to save on edit doctrine event
     *
     * @param mixed         $object       (has to use HistoricTrait)
     * @param array         $originalData
     * @param array         $modifs
     * @param UserInterface $user
     *
     * @return boolean      success or failed
     */
    public function onEdit($object, $originalData, $modifs, $user);

    /**
     * onDelete $object.
     *
     * This function defined historic to save on delete doctrine event
     *
     * @param mixed         $object (has to use HistoricTrait)
     * @param UserInterface $user
     *
     * @return boolean      success or failed
     */
    public function onDelete($object, $user);

    /**
     * buildHistoric.
     *
     * this function is used to switch the context CRUD $action
     * and run right function
     *
     * @param mixed         $object       (has to use HistoricTrait OR subEntityChild which use this trait is defined)
     * @param array         $originalData
     * @param array         $modifs
     * @param UserInterface $user
     * @param string        $action
     *
     * @return boolean      success or failed
     */
    public function buildHistoric($object, $originalData, $modifs, $user, $action);
}

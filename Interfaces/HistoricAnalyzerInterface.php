<?php

namespace Acseo\HistoricBundle\Interfaces;

/**
 * AnalyzerInterface.
 *
 * This interface is used to defined function used
 * to get params from bundle config and analyze it
 */
interface HistoricAnalyzerInterface
{
    /**
     * analyze object depends of action.
     *
     * Compare parameters defined in bundle config with object managed
     * if a class matches, let's instance the service associated
     * which has to implements Acseo\HistoricBundle\Interfaces\HistoricInterface
     * and run function buildHistoric()
     *
     * @param mixed  $object
     * @param string $action
     */
    public function analyze($object, $action);
}
